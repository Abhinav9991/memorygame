const gameContainer = document.getElementById("game")
const results = document.getElementById('result')
const replay = document.getElementById('replay')
const movesCounting = document.getElementById('moves')
const userWon = document.getElementById('won')
const play = document.getElementById('play')
const game = document.getElementById('gameContainer')
const rules = document.getElementById('rules')
let getScore = Number(localStorage.getItem("bestScore"))
const highestScore= document.getElementById("highestScore")
if(getScore===null){
  highestScore.innerHTML=`Best SCore: 0`
}
else{
  highestScore.innerHTML=`Best Score : ${getScore}`
}
console.log(getScore);

const COLORS = [
  "red",
  "blue",
  "palegreen",
  "green",
  "orange",
  "lightsalmon",
  "purple",
  "teal",
  "navy",
  "red",
  "blue",
  "cadetblue",
  "green",
  "lightsalmon",
  "palegreen",
  "orange",
  "purple",
  "navy",
  "cadetblue",
  "teal"
]

function shuffle(array) {
  let counter = array.length

  while (counter > 0) {
    let index = Math.floor(Math.random() * counter)

    counter--

    let temp = array[counter]
    array[counter] = array[index]
    array[index] = temp
  }

  return array
}

let shuffledColors = shuffle(COLORS)

let counter = 0
let firstCard = ""
let secondCard = ""

function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    const newDiv = document.createElement("div")
    newDiv.classList.add(color)
    newDiv.style.backgroundColor = "white"
    newDiv.addEventListener("click", handleCardClick)
    gameContainer.append(newDiv)
    console.log(newDiv)
  }
}
let totalCount = 0
let movesCount = 0
function handleCardClick(event) {


  let color = event.target.className
  if (event.target.style.backgroundColor === "white") {
    event.target.style.backgroundColor = color


  }
  if (counter === 0) {
    firstCard = event.target
    firstCard.removeEventListener("click", handleCardClick)
    counter++
  }
  else {
    secondCard = event.target
    counter = 0
    movesCount++

    if (firstCard.style.backgroundColor !== secondCard.style.backgroundColor || firstCard === secondCard) {
      gameContainer.style.pointerEvents = "none"
      setTimeout(() => {
        firstCard.style.backgroundColor = "white"
        secondCard.style.backgroundColor = "white"
        firstCard.addEventListener("click", handleCardClick)
        secondCard.addEventListener("click", handleCardClick)
        event.preventDefault()
        gameContainer.style.pointerEvents = "auto"
      }, 1000)
    }
    if (firstCard.style.backgroundColor == secondCard.style.backgroundColor && firstCard !== secondCard) {
      console.log("event", firstCard)
      firstCard.style.pointerEvents = "none"
      secondCard.style.pointerEvents = "none"
      totalCount = totalCount + 1
    }
  }

  console.log(movesCount);

  results.innerHTML = "Moves: " + movesCount + " Cards Found: " + totalCount
  if (totalCount === 10) {
    if (localStorage.getItem("bestScore") === null) {
      localStorage.setItem("bestScore",movesCount)
    }
    else if (getScore > movesCount) {
      localStorage.setItem("bestScore", movesCount)
    }
    getScore=localStorage.getItem("bestScore")
    results.innerHTML =  " Your Score:" + movesCount
    userWon.style.display = "block"
    replay.style.display = "block"
  }

}
play.addEventListener('click', () => {
  setTimeout(() => {

    game.style.display = "block"
    results.style.display = "block"
    play.style.display = "none"
    rules.style.display = "none"
  }, 500)

})
createDivsForColors(shuffledColors)